import gitlab
import tomli
import tomli_w
import os
import uuid

import logging
from dotenv import load_dotenv

from fastapi import HTTPException

# ~ token = os.getenv('gitlab_token')
# gl = gitlab.Gitlab(private_token=token)

from datetime import datetime
import requests

from app_functions import get_file_name_from_url


logger = logging.getLogger(__name__)

load_dotenv()
dict_a = {}

src_project_id = os.getenv("SRC_PROJECT_ID")
dict_a["tgt_project_id"] = tgt_project_id = os.getenv("TGT_PROJECT_ID")
dict_a["urls_file"] = os.getenv("urls_file")
gitlab_token = os.getenv("gitlab_token")
target_branch_name = os.getenv("target_branch_name")
user_agent = os.getenv("user_agent")

gl = gitlab.Gitlab(private_token=gitlab_token)


def create_branch(project, branch_name, ref_branch):
    """Create a branch given a project, branch_name and a reference branch"""
    try:
        branch = project.branches.create(
            {
                "branch": branch_name,
                "ref": project.default_branch,
            }  # ref was src_project.default_branch
        )
        return branch
    except gitlab.exceptions.GitlabCreateError:
        logging.debug("Unable to create branch")
        raise
    except Exception as e:
        logging.debug(f"Unable to create branch {e}")
        raise e


def create_file(toml_content, dict_a):
    """Create a file given content and dictionary of details"""
    project = dict_a["src_project"]
    try:
        project.files.create(
            {
                "file_path": dict_a["file_name"],
                "branch": dict_a["src_branch"],
                "encoding": dict_a["encoding"],  # or 'base64'; useful for binary files
                # ~ "author_email": AUTHOR_EMAIL, # Optional
                # ~ "author_name": AUTHOR_NAME,  # Optional
                "commit_message": dict_a["title"],
                "content": toml_content,
            }
        )
    except Exception as e:
        logging.debug(f"Unable to create file: {e}")
        raise e


def create_merge_request(dict_a: dict):
    """Create merge request from the provided dectionary data"""
    project = dict_a["src_project"]
    try:
        merge_request = project.mergerequests.create(
            {
                "source_branch": dict_a["src_branch"],  # was branch.name
                "target_project_id": dict_a["tgt_project_id"],
                "target_branch": dict_a[
                    "tgt_project"
                ].default_branch,  # was tgt_project.default_branch,
                "title": dict_a["title"],
            }
        )
        return merge_request
    except Exception as e:
        logging.debug(f"create_merge_request: Exception: {e}")
        raise e


def edit_file(new_content, dict_a):
    """Edit file with new content"""
    project = dict_a["src_project"]
    try:
        f = project.files.get(file_path=dict_a["urls_file"], ref=dict_a["ref_branch"])

        # ~ decoded_content = f.decode()
        # update the contents and commit the changes
        f.content = new_content
        f.save(branch=dict_a["src_branch"], commit_message=dict_a["title"])
    except Exception as e:
        logging.debug(f"edit_file: Exception: {e}")
        raise e


def get_raw_file(project, path_to_file):
    """Returns raw file from a given project"""
    try:
        return project.files.raw(file_path=path_to_file, ref=project.default_branch)
    except Exception as e:
        logging.debug(f"get_raw_file: Exception: {e}")
        raise e


# ~ def update_file(project, path_to_file, raw_content):
    # ~ try:
        # ~ raw_content = project.files.raw(file_path=file_path, ref=project.default_branch)
    # ~ except Exception as e:
        # ~ logging.debug(f"update_file: Exception: {e}")
        # ~ raise e


def append_url(raw_content, new_url):
    """Loads raw file, adds a URL and return a dictionary"""
    toml_dict = tomli.loads(raw_content.decode())
    toml_dict["urls"].append(new_url)
    return toml_dict


def download_new_urls(urls_file, data_dir):
    # Read the TOML file
    count = 0
    with open(urls_file, "r") as file:
        data = tomli.load(file)
        logger.info("download_new_urls: loading urls file")

    # Check if the completed_urls key exists, if not create it
    if "completed_urls" not in data:
        data["completed_urls"] = []
        logger.info("download_new_urls: Checked if completed_urls in data")
    else:
        data["completed_urls"] = data["completed_urls"]

    # Copy list
    url_list = data["urls"].copy()

    # Iterate through the urls and download them if they do not already exist
    for url in url_list:
        count += 1
        filename = url.split("/")[-1]
        response = requests.get(url)
        logger.info(f"download_new_urls: Requested {url}")
        with open(f"{data_dir}{filename}", "wb") as file:
            file.write(response.content)
            logger.info(f"download_new_urls: saved {filename}")
        data["completed_urls"].append(url)
        logger.info(f"download_new_urls: added {url} to list of completed url")
        data["urls"].remove(url)
        logger.info(f"download_new_urls: Removed {url} from list of urls")
        logger.info(f"download_new_urls: Count =  {count}")

    # Uniqe values only
    myset = set(data["completed_urls"])
    data["completed_urls"] = list(myset)

    # Sort values
    data["completed_urls"].sort()

    # Update the TOML file with the completed_urls list
    with open(urls_file, "w") as file:
        tomli_w.dump(data, file)
        logging.info("saved new data to urls.toml file")


class API:
    dict_a["src_project"] = src_project = gl.projects.get(src_project_id)
    dict_a["tgt_project"] = tgt_project = gl.projects.get(tgt_project_id)
    dict_a["tgt_branch"] = target_branch_name = os.getenv("target_branch_name")

    def do_create_mr(self, toml_content):
        """
        Create the Merge Request using data from the Class
        """
        try:
            # Generate random id and set it as branch name
            dict_a["src_branch"] = lazy_id = f"lazy-{str(uuid.uuid4())}"
            # Use the random id for file name
            dict_a["file_name"] = f"freelancers/toml/{lazy_id}.toml"
            dict_a["title"] = f"Add {lazy_id}"  # title for commit message
            dict_a["ref_branch"] = "ref_test_branch"
            dict_a["encoding"] = "text"
            create_branch(
                dict_a["src_project"], dict_a["src_branch"], dict_a["ref_branch"]
            )
            create_file(toml_content, dict_a)
            return create_merge_request(dict_a)

        except gitlab.GitlabCreateError as e:
            logger.error("create_mr: unable to create MR")
            raise HTTPException(status_code=400, detail=str(e))
        except Exception as e:
            raise HTTPException(status_code=400, detail=str(e))

    def is_valid_remote_toml(self, url):
        """Validate that remote url to file has valid toml content"""
        try:
            assert url.startswith("http"), "URL should start with http"
            assert url.endswith(".toml"), "URL should end with .toml"
            logger.info(f"is_valid_remote_toml: URL is: {url}")
            request = requests.get(url)
            tomli.loads(request.text)
            return True
        except Exception as e:
            logger.error(f"validate_remote_toml: URL does not contain valid toml: {e}")
            return False

    def check_valid_toml(self, data: str):
        """Checked if the provided string is a valid toml"""
        try:
            tomli.loads(data)
        except tomli.TOMLDecodeError:
            logging.error("Yep, definitely not valid toml.")

    def is_url_image(self, image_url: str):
        "Check if URL is an image URL"
        image_formats = ("image/png", "image/jpeg", "image/jpg")
        try:
            headers = {"User-Agent": user_agent}
            r = requests.head(image_url, headers=headers)
            if r.headers["content-type"] in image_formats:
                return True
            return False
        except Exception as e:
            logger.error(f"is_url_image: Unable to check image link: {e}")
            return False

    def update_url_file(self, url):
        """Update URLs file"""
        try:
            old_url_file = get_raw_file(dict_a["tgt_project"], dict_a["urls_file"])
            new_data = append_url(old_url_file, url)
            toml_content = tomli_w.dumps(new_data)
            url_dict = tomli.loads(old_url_file.decode())
            print(url_dict)
            print(url_dict["completed_urls"])
            if url not in url_dict["completed_urls"]:
                branch_name = get_file_name_from_url(
                    url
                )  # used to create branch name from url
                logger.debug(f"update_url_file: {branch_name=}")
                commit_message = f"Add {branch_name}"
            else:
                now = datetime.now()
                time = now.strftime("%Y-%m-%d_%H-%M")
                branch_name = get_file_name_from_url(
                    url
                )  # used to create branch name from url
                branch_name = branch_name + "_" + time
                logger.debug(f"update_url_file: Existing URL addition {branch_name=}")
                commit_message = f"Add {branch_name} update {time}"
            dict_a["src_branch"] = branch_name
            dict_a["src_branch"] = branch_name
            dict_a["title"] = commit_message
            dict_a["ref_branch"] = "main"
            logger.debug(f"update_url_file: Creating branch")
            create_branch(
                dict_a["src_project"], dict_a["src_branch"], dict_a["ref_branch"]
            )
            # Update file with toml_content
            edit_file(toml_content, dict_a)
            logger.debug(f"update_url_file: Creating merge request")
            return create_merge_request(dict_a)

        except AssertionError as e:
            error_str = f"update_url_file: Assertion error - {e}"
            logger.error(error_str)
            raise HTTPException(status_code=400, detail=error_str)
        except gitlab.exceptions.GitlabCreateError as e:
            raise HTTPException(status_code=400, detail=str(e))
