import logging

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from routes.route import router

import os

from dotenv import load_dotenv

# Use .env files
load_dotenv()

log_level = os.getenv("log_level")

if log_level == "debug":
    logging.basicConfig(level=logging.DEBUG)

logger = logging.getLogger(__name__)
# logging.getLogger().setLevel(logging.DEBUG)


app = FastAPI()

app.include_router(router)

# CORS origions
origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://127.0.0.1:5174",
    "https://hur-project.gitlab.io",
    "http://localhost:5173",
]

app.add_middleware(
    CORSMiddleware,
    # ~ allow_origins=origins,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
# End CORS Section #
