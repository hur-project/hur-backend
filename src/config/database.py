# import the MongoClient class
from pymongo import MongoClient, errors
import pymongo

from dotenv import load_dotenv

load_dotenv()
from os import getenv

import logging

logger = logging.getLogger(__name__)

# Import global variables for MongoDB host (default port is 27017) from Env.
db_host = getenv("db_host")
db_port = getenv("db_port")
db_user = getenv("db_user")
db_pass = getenv("db_pass")

# use a try-except indentation to catch MongoClient() errors
try:
    logger.debug(f"Trying to initate MongoClient")
    # try to instantiate a client instance
    client = MongoClient(
        host=[str(db_host) + ":" + str(db_port)],
        serverSelectionTimeoutMS=3000,  # 3 second timeout
        username=db_user,
        password=db_pass,
        authMechanism="PLAIN",
    )

    # print the version of MongoDB server if connection successful
    print("server version:", client.server_info()["version"])

    # get the database_names from the MongoClient()
    logger.debug(f"Listing databases")
    database_names = client.list_database_names()

except errors.ServerSelectionTimeoutError as err:
    # set the client and DB name list to 'None' and `[]` if exception
    client = None
    database_names = []

    # catch pymongo.errors.ServerSelectionTimeoutError
    print("pymongo ERROR:", err)

print("\ndatabases:", database_names)

logger.debug(f"Connecting to json_database")
db = client.json_database


collection = db.json_collection

# ~ collection.create_index(
    # ~ {
        # ~ "services": "text",
        # ~ "data": "text",
    # ~ }
# ~ )
