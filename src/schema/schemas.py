def individual_serial(user_profile) -> dict:
    return {
        "id": str(user_profile["_id"]),
        "tags": user_profile["tags"],
        "services": user_profile["services"],
        "data": user_profile["data"],
        "file_name": user_profile["file_name"],
    }


def list_serial(user_profiles) -> list:
    return [individual_serial(user_profile) for user_profile in user_profiles]
