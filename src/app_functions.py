from urllib.parse import urlparse
import os


def reverse_domain_name(domain):
    parts = domain.split(".")
    reversed_domain = ".".join(parts[::-1])
    return reversed_domain


def extract_domain(url):
    parsed_url = urlparse(url)
    return parsed_url.netloc


def get_reverse_domain_from_url(url):
    domain = extract_domain(url)
    return reverse_domain_name(domain)


def get_file_name_from_url(url):
    a = urlparse(url)
    return os.path.basename(a.path)
