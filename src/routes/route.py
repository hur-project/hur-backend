from fastapi import APIRouter, HTTPException, Response, status
from models.api import Model, TomlUrl
from config.database import collection
from schema.schemas import list_serial, individual_serial
from bson import ObjectId

import gitlab
import logging
import os

import tomli
import tomli_w

from git_api import API

# Initiate instance of class
api_class = API()

log_level = os.getenv("log_level")
if log_level == "debug":
    logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


router = APIRouter()
dict_a = {}
production = eval(os.getenv("production"))


@router.post("/ur_data/")
async def create_item(ur_data: Model):
    "Endpoint for submitting toml file to backend"
    try:
        data_dict = ur_data.dict()
        # FIXME: check why we do this
        data_dict["data"]["profile_image"] = str(data_dict["data"]["profile_image"])

        toml_content = tomli_w.dumps(data_dict)

        toml_dict = tomli.loads(toml_content)
        profile_img = toml_dict["data"]["profile_image"]
        is_image = api_class.is_url_image(profile_img)
        if production and is_image:
            logger.info("create_item: it is url!")
            merge_request_url = api_class.do_create_mr(toml_content)
            logger.info("create_item: Response with link!")
            return {"status": "ok", "web_url": merge_request_url.web_url}
        elif not production:
            logger.info("create_item: Not production!")
            return {"status": "ok", "web_url": "http://not.production.com/"}
        else:
            logger.error("create_item: link not an image!")
            raise HTTPException(
                status_code=400, detail="Link does not seem to be an image"
            )
    except gitlab.exceptions.GitlabCreateError as e:
        logger.error(f"create_item: GitlabCreateError exception! {e}")
        raise HTTPException(status_code=400, detail=str(e))
    except gitlab.exceptions.GitlabAuthenticationError as e:
        logger.error(f"create_item: GitlabAuthenticationError exception {e}!")
        raise HTTPException(status_code=400, detail=str(e))
    except Exception as e:
        logger.error(f"create_item: General Exception {e}")
        raise HTTPException(status_code=400, detail=str(e))


@router.post("/submit_url/")
async def submit_url(toml_url: TomlUrl, response: Response):
    """Hanldes URL submission, updates the URLs file with the URL submitted"""
    data_dict = toml_url.dict()
    url = str(data_dict["url"])
    try:
        if api_class.is_valid_remote_toml(url):
            pass
        else:
            response.status_code = status.HTTP_400_BAD_REQUEST
            return {"status": 400, "data": "File not a toml file"}
        if production:
            update_file_response = api_class.update_url_file(url)

            if isinstance(
                update_file_response,
                gitlab.v4.objects.merge_requests.ProjectMergeRequest,
            ):
                logger.error("submit_url: It is MR!")
                return {"status": "ok", "web_url": update_file_response.web_url}
        else:
            logger.info("submit_url: Not production!")
            return {"status": "ok", "web_url": "http://not.production.com/"}
    except gitlab.exceptions.GitlabCreateError as e:
        logger.error(f"submit_url: GitlabCreateError exception! {e}")
        raise HTTPException(status_code=400, detail=str(e))
    except gitlab.exceptions.GitlabAuthenticationError as e:
        logger.error(f"submit_url: GitlabAuthenticationError exception! {e}")
        raise HTTPException(status_code=400, detail=str(e))
    except Exception as e:
        logger.error(f"submit_url: General Exception {e}")
        raise HTTPException(status_code=400, detail=str(e))


@router.post("/{id}")
async def get_user_profile_by_id(id: str):
    """Get profile by ID"""
    user_profile = individual_serial(collection.find_one({"_id": ObjectId(id)}))
    return user_profile


@router.get("/tags/{tag}")
def search_tag(tag: str):
    """Search profiles by tag"""
    result = list_serial(collection.find({"tags": tag}))
    if result:
        return {"message": "Found", "data": result}
    else:
        return {"message": "Not found"}


# ~ # POST Request Method
# ~ @router.post("/")
# ~ async def post_todo(todo: Todo):
    # ~ collection_name.insert_one(dict(todo))

# ~ # PUT request method
# ~ @router.put("/{id}")
# ~ async def put_todo(id: str, todo: Todo):
    # ~ collection_name.find_one_and_update({"_id": ObjectId(id)}, {
        # ~ "$set": dict(todo)
    # ~ })
# ~ # Delete request method
# ~ @router.delete("/{id}")
# ~ async def delete_todo(id: str):
    # ~ collection_name.find_one_and_delete({"_id": ObjectId(id)})
