from pydantic import BaseModel, Field, TypeAdapter, AnyUrl, BeforeValidator

from typing import List
from typing_extensions import Annotated


AnyUrlTypeAdapter = TypeAdapter(AnyUrl)

# Custom type for string URL handling
AnyUrlStr = Annotated[
    str,
    BeforeValidator(lambda value: AnyUrlTypeAdapter.validate_python(value) and value),
]


class Data(BaseModel):
    name: str
    website: List = Field(min_length=1)
    profile_image: AnyUrlStr
    user_description: str
    payment_methods: List[str]
    hourly_rate: int


class Service(BaseModel):
    category: str
    sub_category: str
    title: str
    description: str
    price: int
    service_image: List[str]
    id: str


class Model(BaseModel):
    file_format: str
    data: Data
    services: List[Service] = Field(min_length=1)
    tags: List[str]


class TomlUrl(BaseModel):
    url: AnyUrl
    # ~ url: str
