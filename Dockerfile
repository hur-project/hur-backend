FROM python:3.11
COPY requirements.txt app/requirements.txt
WORKDIR /app
RUN whoami
RUN ls -la
RUN pwd
RUN pip install -r requirements.txt
COPY src/ /app
EXPOSE 8000
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000" ,"--reload", "--proxy-headers", "--forwarded-allow-ips='*'"]
