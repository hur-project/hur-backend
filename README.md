# Hur API

This is the backend to handle profile link and direct user profile (lazy) submission.

## Run the server

Example command to run the server with self singed certificate:

```
uvicorn main:app --reload --port 8085 --host example.com --ssl-certfile=certificate.pem --ssl-keyfile=key.pem --log-level debug
```

## License 

AGPL-V3
